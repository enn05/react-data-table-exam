import React, {  } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input,  } from 'reactstrap';

const UserModal = (props) => {
  const {
    toggle,
    modal, handleEmailChange, handleNameChange, handleUsernameChange, handleSaveUser,
  } = props;

  return (
    <div>
      <Modal isOpen={modal} toggle={toggle} className={''}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup className="mb-1">
              <Label for="name">Name</Label>
              <Input type="text" name="name" id="name" placeholder="Name" onChange={handleNameChange}/>
            </FormGroup>
            <FormGroup className="mb-1">
              <Label for="username">Username</Label>
              <Input type="text" name="username" id="username" placeholder="Username" onChange={handleUsernameChange} />
            </FormGroup>
            <FormGroup className="mb-1">
              <Label for="email">Email</Label>
              <Input type="email" name="email" id="email" placeholder="Email" onChange={handleEmailChange} />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={handleSaveUser}>Save</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default UserModal;