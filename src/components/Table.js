import React, {useState, useEffect} from 'react';
import DataTable from 'react-data-table-component';
import { Button, Card, CardBody } from 'reactstrap';
import UserModal from './Modal';
import moment from 'moment';
import Swal from 'sweetalert2';


const Table = (props) => {
  const [users, setUsers] = useState({})
  const [modal, setModal] = useState(false);
  const [name, setName] = useState('')
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [emailValid, setEmailValid] = useState(false);
  const [emailExist, setEmailExist] = useState(false)
  const [usernameExist, setUsernameExist] = useState(false)

  const toggle = () => {
    setModal(!modal)
    handleRefresh()
  };

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(data => {
        setUsers(data);
      });
  }, [])

  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true
    },
    {
      name: "Username",
      selector: "username",
      sortable: true
    },
    {
      name: "Email",
      selector: "email",
      sortable: true,
      right: true
    }
  ];

  const action = (
    <Button className="primary" size="sm" onClick={toggle}>Add</Button>
  )

  const handleRefresh = () => {
    setEmail('')
    setName('')
    setUsername('')
    setEmailValid(false)
  }

  const handleEmailChange = e => {
    const emailVal = e.target.value.trim()
    setEmail(emailVal)

    if (!users.filter(item=> item.email === emailVal).length == 0){
      setEmailExist(true)
    }else{
      setEmailExist(false)
    }

    const emailPattern = new RegExp(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
    let result = emailPattern.test(emailVal)
    if(result){
      setEmailValid(true);
    }else{
      setEmailValid(false)
    }

  }
  const handleNameChange = e => {
    const nameVal = e.target.value.trim()
    setName(nameVal)
  }
  const handleUsernameChange = e => {
    const usernameVal = e.target.value.trim()
    setUsername(usernameVal)
    if (!users.filter(item=> item.username === usernameVal).length == 0){
      setUsernameExist(true)
    }else{
      setUsernameExist(false)
    }
  }

  const handleSaveUser = () => {
    console.log(usernameExist)
    if(!name || !username || !email){
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'All fields are required!',
      })
    }
    if(!emailValid){
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please input valid email!',
      })
    }
    if(emailExist){
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Email already exists!',
      })
    }
    if(usernameExist){
      return Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Username already exists!',
      })
    }
    let id = moment(new Date()).format('x')
    let newUsers = [...users]
    newUsers.unshift({id, name, username, email})
    setUsers(newUsers)
    Swal.fire(
      'Success!',
      'New user added.',
      'success'
    )
    handleRefresh()
    toggle()

  }


  return (
    <div>
      <Card>
        <CardBody>
          <UserModal 
            buttonLabel={'Add'}
            modal={modal}
            toggle={toggle}
            handleEmailChange={handleEmailChange}
            handleNameChange={handleNameChange}
            handleUsernameChange={handleUsernameChange}
            handleSaveUser={handleSaveUser}
          />
          <DataTable
            title="Users"
            columns={columns}
            data={users}
            defaultSortField="title"
            pagination
            actions={action}
          />
        </CardBody>
      </Card>
    </div>
  );
};

export default Table;