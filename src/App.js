import './App.css';
import Table from './components/Table';
import React from 'react';

function App() {
  return (
    <React.Fragment>
      <div className="" style={{ margin: '100px' }}>
        <Table />
      </div>
    </React.Fragment>
  );
}

export default App;
